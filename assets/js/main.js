$(document).ready(function(){
    //apply the scroll class to the body in order to keep the navigation on top of screen when pass 45px
    $(window).on("scroll",function(e){
        var scr = $(window).scrollTop(),
            $body = $("body");
        if (scr >= 45){
            $body.addClass("scroll");
        }else{
            $body.removeClass("scroll");
        }
    });
    //if no cookie present on page
    if(!Cookies.get('landingModal')){
        var $modal = $(".modal"),
            $form = $modal.find(".form");
        $.ajax({
            url: "formFields.php",
            context: $modal,
            dataType: "json",
            success: function(data){
                //asynchronous request on formFields.php and parse response to create inputs
                $.each(data, function(i,d) {
                    var $input = $("<input />").attr("type","text"),
                        $label = $("<label></label>"),
                        $div = $("<div></div>").attr("class","form-input");
                    //HTML5 compliance to get simple email verification
                    if(d.name == "email"){
                        $input.attr("type",d.name);
                    }
                    //HTML5(Safari only) compliance to get only numbers if used on phone
                    if(d.name == "telephone"){
                        $input.attr("type","tel");
                    }
                    //append label (with `for` attribute for click on label) then input (with `name` and `id` attributes) to container `form-input`
                    $div.append($label.attr("for",d.name).text(d.label),$input.attr({"name":d.name,"id":d.name}));
                    //append the container to the form
                    $form.append($div);
                });
                //append submit button
                var $submit = $("<input />").attr({"type":"submit","value":"send"});
                $form.append($submit);
                
                //once all fields are prepared, show the modal
                $(this).fadeIn(200);

                //on form submit,..
                $form.on("submit",function(e){
                    //prevent automatic form action
                    e.preventDefault();
                    var $form = $(this),
                        url = $form.attr('action'),
                        type = $form.attr('method');
                    $.ajax({
                        type: type,
                        url: url,
                        data: $form.serialize(),
                        success: function(data){
                            //send asynchronously the content of each inputs to formSubmit.php and display response
                            console.log(data);
                            //hide the modal 
                            $modal.fadeOut(200);
                        },
                        error: function(data){
                            console.warn("The form has not been sent due to problem with the database. Please contact admin.");
                        }
                    });
                });
                //on modal's background click,..
                $(this).children(".layer").on("click",function(e){
                    //hide the modal
                    $modal.fadeOut(200);
                });
            },
            error: function(data){
                console.warn("There is an error with the database. Can't show the contact form. Please contact admin.");
            }
        });
        //once the modal has been shown, can't be seen again (except if reset the cookies)
        Cookies.set('landingModal', 1);
    }
});