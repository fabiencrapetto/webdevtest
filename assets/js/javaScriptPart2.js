var a = {
	greeting: 'I found you, ',
	say: function() {
		//since `this` is used in the jQuery.each function, transfer `this` object into a new variable `that`
		var that = this;
		jQuery.each(this.names, function(index, value) {
			alert(that.greeting + value);
		});
	}
};

var b = {
	names: ['Ciri', 'Geralt', 'Triss', 'Vesemir']
};

/* Merge the objects into a single object here */
var c = { ...a, ...b };

c.say();

