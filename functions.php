<?php
    //connect to mysqli DB using separated setup file
    function dbConnect(){
        $config = parse_ini_file('../db.ini');
        $con = mysqli_connect("localhost",$config['username'],$config['password'],$config['db']);
        if(!$con){
            die("Failed to connect to Database"); 
        }
        return $con;
    }
?>