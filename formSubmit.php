<?php
    require_once "functions.php";

    //check if POST available
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $result = array();
        //parse through POST global to get all non-empty fields and add it to result array
        foreach( $_POST as $key => $value){
            if(!empty($value)){
                $result[$key] = $value;
            }
        }
    }
    //check if result is not empty (at least 1 input filled)
    if( !empty($result)){
        //connect to mysql
        $conn = dbConnect();
        //insert today's date in formSubmit
        $sql = "INSERT INTO formSubmit (`date`) VALUES (now())";

        if ($conn->query($sql) === TRUE) {
            //get formSubmit ID
            $last_id = $conn->insert_id;
            //prepare insert for secure query
            $stmt = $conn->prepare("INSERT INTO formSubmitData (`formSubmit_id`,`name`,`value`) VALUES (?,?,?)");
            //bind parameters to prepared statement
            $stmt->bind_param('iss',$id,$name,$value);
            //parse through filled inputs
            foreach( $result as $k => $v){
                //bind values to prepared bind parameters
                $id = $last_id;
                $name = $k;
                $value = $v;
                if ($stmt->execute() === TRUE) {
                    //browser console outputs each value for transparency with formSubmit Foreign Key 
                    echo "The input '$name' with value '$value' inserted in DB. FK:$id \n";
                } else {
                    echo "Error: " .$stmt. "\n" .$stmt->errorCode();
                }
            }
        } else {
            echo "Error:" .$sql. "\n" .$conn->error;
        }
        //close mysql connection
        $conn->close();
    }else{
        echo "Fields were all empty, nothing has been saved in DB.";
    }
?>