<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
?>

<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<title>Fabien Crapetto - CLBS Webtest for Fullstack Developer</title>
		<meta name="description" content="This page is my (Fabien Crapetto) entry test at CLBS company for a Fullstack Developer position">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/main.css">
	</head>
	
	<body>
		<nav class="navbar">
			<div class="container">
				<ul>
					<li><a href="/">home</a></li>
					<li><a href="/#news">news</a></li>
					<li><a href="/#imprint">imprint</a></li>
				</ul>
			</div>
		</nav>
		<main class="content">
			<div class="container">
				<header>
					<img src="assets/images/birds.jpg" height="356px" alt="header birds"/>
				</header>
				<div class="flex">
					<div class="text">
						<p>The mate was a mighty sailin' man the Skipper brave and sure. Five passengers set sail that day for a three hour tour a three hour tour. The weather started getting rough - the tiny ship was tossed. If not for the courage of the fearless crew the Minnow would be lost. the Minnow would be lost. Here's the story of a lovely lady who was bringing up three very lovely girls.</p>
						<p>Baby if you've ever wondered - wondered whatever became of me. I'm living on the air in Cincinnati. Cincinnati WKRP. Their house is a museum where people come to see ‘em. They really are a scream the Addams Family. The weather started getting rough - the tiny ship was tossed. If not for the courage of the fearless crew the Minnow would be lost. the Minnow would be lost. We're gonna do it. On your mark get set and go now. Got a dream and we just know now we're gonna make our dream come true? It's time to play the music. It's time to light the lights. It's time to meet the Muppets on the Muppet Show tonight.</p>
						<p>Sunny Days sweepin' the clouds away. On my way to where the air is sweet. Can you tell me how to get how to get to Sesame Street. So get a witch's shawl on a broomstick you can crawl on. Were gonna pay a call on the Addams Family. Makin their way the only way they know how. That's just a little bit more than the law will allow? Come and dance on our floor. Take a step that is new. We've a loveable space that needs your face threes company too. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest.</p>
						<p>If you have a problem if no one else can help and if you can find them maybe you can hire The A-Team! The weather started getting rough - the tiny ship was tossed. If not for the courage of the fearless crew the Minnow would be lost. the Minnow would be lost. Texas tea. All of them had hair of gold like their mother the youngest one in curls.</p>
						<p>Doin' it our way. There's nothing we wont try. Never heard the word impossible. This time there's no stopping us! Makin' your way in the world today takes everything you've got. Takin' a break from all your worries sure would help a lot. Fleeing from the Cylon tyranny the last Battlestar – Galactica - leads a rag-tag fugitive fleet on a lonely quest - a shining planet known as Earth. Makin their way the only way they know how. That's just a little bit more than the law will allow. The Brady Bunch the Brady Bunch that's the way we all became the Brady Bunch! So lets make the most of this beautiful day. Since we're together. So get a witch's shawl on a broomstick you can crawl on. Were gonna pay a call on the Addams Family. But they got diff'rent strokes. It takes diff'rent strokes - it takes diff'rent strokes to move the world. Goodbye gray sky hello blue. There's nothing can hold me when I hold you. Feels so right it cant be wrong. Rockin' and rollin' all week long. The movie star the professor and Mary Ann here on Gilligans Isle. Today still wanted by the government they survive as soldiers of fortune. And we'll do it our way yes our way. Make all our dreams come true for me and you.</p>
					</div>
					<div class="pic">
						<img class="img-responsive" src="assets/images/flowers.jpg" alt="pic flowers" />
					</div>
				</div>
			</div>
		</main>
		<div class="modal">
			<div class="layer"></div>
			<div class="content">
				<div class="flex">
					<div class="text">
						<h1>Welcome</h1>
						<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Curabitur blandit tempus porttitor.</p>
						<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
					</div>
					<form class="form" action="formSubmit.php" method="post">
						<h2>Contact Form</h2>
					</form>
				</div>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
		<script src="assets/js/main.js"></script>
		<script src="assets/js/javaScriptPart2.js"></script>
	</body>

</html>