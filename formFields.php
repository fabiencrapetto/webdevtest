<?php
	require_once "functions.php";
	//connect to DB
	$conn = dbConnect();

	//select fields from formFields
	$sql = "SELECT name, label FROM formFields";
	$result = $conn->query($sql);

	$modalFields = array();
	if ($result->num_rows > 0) {
		//parse through result and associate values in array modalFields
		while($row = $result->fetch_assoc()) {
			$name = $row["name"];
			$label = $row["label"];
			$modalFields[] = array(
				'name' => $name,
				'label' => $label
			);
		}
	} else {
		echo false;
	}
	//close mysql connection
	$conn->close();
	
	//send modalFields array as json for jQuery
	echo json_encode($modalFields);
?>
